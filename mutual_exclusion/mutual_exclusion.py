"""Two shoppers adding items to a shared notepad"""

import threading
import time

garlic_count = 0
pencil = threading.Lock()


def shopper():
    global garlic_count
    # pencil.acquire()
    for i in range(5):
        print(f"{threading.current_thread().getName()} is thinking.")
        time.sleep(0.5)
        pencil.acquire()
        garlic_count += 1
        pencil.release()
    # pencil.release()

if __name__ == "__main__":
    thread1 = threading.Thread(target=shopper)
    thread2 = threading.Thread(target=shopper)
    thread1.start()
    thread2.start()
    thread1.join()
    thread2.join()
    print(f"Final garlic count: {garlic_count}")
