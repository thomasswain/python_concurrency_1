"""Two shoppers adding items to a shared notepad"""

import threading

garlic_count = 0


def shopper():
    global garlic_count
    for i in range(10000000):
        garlic_count += 1


if __name__ == "__main__":
    thread1 = threading.Thread(target=shopper)
    thread2 = threading.Thread(target=shopper)
    thread1.start()
    thread2.start()
    thread1.join()
    thread2.join()
    print(f"Final garlic count: {garlic_count}")
