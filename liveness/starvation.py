"""Three philosophers, thinking and eating sushi"""

import threading

chopstick_a = threading.Lock()
chopstick_b = threading.Lock()
chopstick_c = threading.Lock()
sushi_count = 5000


def philosopher(name, first_chopstick, second_chopstick):
    global sushi_count
    sushi_eaten = 0
    while sushi_count > 0:
        with first_chopstick:
            with second_chopstick:
                if sushi_count > 0:
                    sushi_count -= 1
                    sushi_eaten += 1
                    print(f"{name} took a piece. Sushi remaining: {sushi_count}")
    print(f"{name} took {sushi_eaten} pieces.")


if __name__ == "__main__":
    # Commented code results in Philosopher 2 getting most of the sushi, because 1/3 are both competing for chopstick_a
    # threading.Thread(target=philosopher, args=("Philosopher 1", chopstick_a, chopstick_b)).start()
    # threading.Thread(target=philosopher, args=("Philosopher 2", chopstick_b, chopstick_c)).start()
    # threading.Thread(target=philosopher, args=("Philosopher 3", chopstick_a, chopstick_c)).start()

    # Below results in starvation for the later threads
    for thread in range(50):
        threading.Thread(target=philosopher, args=(f"Philosopher {thread}-1", chopstick_a, chopstick_b)).start()
        threading.Thread(target=philosopher, args=(f"Philosopher {thread}-2", chopstick_a, chopstick_b)).start()
        threading.Thread(target=philosopher, args=(f"Philosopher {thread}-3", chopstick_a, chopstick_b)).start()
