"""Three philosophers, thinking and eating sushi"""

import threading
import time
from random import random

chopstick_a = threading.Lock()
chopstick_b = threading.Lock()
chopstick_c = threading.Lock()
sushi_count = 5000


def philosopher(name, first_chopstick, second_chopstick):
    global sushi_count
    while sushi_count > 0:
        first_chopstick.acquire()
        if not second_chopstick.acquire(blocking=False):
            print(f"{name} release their first chopstick.")
            first_chopstick.release()
            # Without below sleep, livelock will occur
            time.sleep(random()/10)
        else:
            if sushi_count > 0:
                sushi_count -= 1
                print(f"{name} took a piece. Sushi remaining: {sushi_count}")
            second_chopstick.release()
            first_chopstick.release()


if __name__ == "__main__":
    threading.Thread(target=philosopher, args=("Philosopher 1", chopstick_a, chopstick_b)).start()
    threading.Thread(target=philosopher, args=("Philosopher 2", chopstick_b, chopstick_c)).start()
    threading.Thread(target=philosopher, args=("Philosopher 3", chopstick_c, chopstick_a)).start()
