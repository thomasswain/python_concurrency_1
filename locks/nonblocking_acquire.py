"""Two shopper adding items to a shared notepad"""

import threading
import time

items_on_notepad = 0
pencil = threading.Lock()


def shopper():
    global items_on_notepad
    name = threading.current_thread().getName()
    items_to_add = 0
    while items_on_notepad <= 20:
        # Commented lines result in running ~3 times slower overall
        # if items_to_add:
        if items_to_add and pencil.acquire(blocking=False):
            # pencil.acquire()
            items_on_notepad += items_to_add
            print(f"{name} added {items_to_add} item(s) to notepad.")
            items_to_add = 0
            time.sleep(0.3)
            pencil.release()
        else:
            time.sleep(0.1)
            items_to_add += 1
            print(f"{name} found something else to buy.")


if __name__ == "__main__":
    shopper1 = threading.Thread(target=shopper, name="Shopper 1")
    shopper2 = threading.Thread(target=shopper, name="Shopper 2")
    start_time = time.perf_counter()
    shopper1.start()
    shopper2.start()
    shopper1.join()
    shopper2.join()
    elapsed_time = time.perf_counter() - start_time
    print(f"Elapsed Time: {elapsed_time:.2f} seconds.")
