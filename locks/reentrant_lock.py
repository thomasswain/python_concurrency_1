"""Two shoppers adding garlic and potatoes to a shared notepad"""

import threading

garlic_count = 0
potato_count = 0
pencil = threading.RLock()
# Using normal Lock instead causes deadlock as attempt to acquire blocks until release
# pencil = threading.Lock()


def add_garlic():
    global garlic_count
    pencil.acquire()
    garlic_count += 1
    pencil.release()


def add_potato():
    global potato_count
    pencil.acquire()
    potato_count += 1
    add_garlic()
    pencil.release()


def shopper():
    for i in range(10_000):
        add_garlic()
        add_potato()


if __name__ == "__main__":
    thread1 = threading.Thread(target=shopper())
    thread2 = threading.Thread(target=shopper())
    thread1.start()
    thread2.start()
    thread1.join()
    thread2.join()
    print(f"Total garlic count: {garlic_count}")
    print(f"Total potato count: {potato_count}")
