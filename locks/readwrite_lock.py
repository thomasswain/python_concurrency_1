"""Several users reading a calendar, but only a few users updating it"""

import threading
from readerwriterlock import rwlock

WEEKDAYS = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
today = 0
# marker = threading.Lock()
marker = rwlock.RWLockFair()


def calendar_reader(id_number):
    global today
    name = f"Reader-{str(id_number)}"
    read_marker = marker.gen_rlock()
    while today < len(WEEKDAYS) - 1:
        # marker.acquire()
        read_marker.acquire()
        print(f"{name} sees that today is {WEEKDAYS[today]}. Read Count: {read_marker.c_rw_lock.v_read_count}\n")
        # marker.release()
        read_marker.release()


def calendar_writer(id_number):
    global today
    name = f"Writer-{str(id_number)}"
    write_marker = marker.gen_wlock()
    while today < len(WEEKDAYS) - 1:
        # marker.acquire()
        write_marker.acquire()
        today = (today + 1) % 7
        print(f"{name} updated date to {WEEKDAYS[today]}.\n")
        # marker.release()
        write_marker.release()


if __name__ == "__main__":
    # Create 10 reader threads
    for i in range(10):
        threading.Thread(target=calendar_reader, args=(i,)).start()
    # Create 2 writer threads
    for i in range(2):
        threading.Thread(target=calendar_writer, args=(i,)).start()
