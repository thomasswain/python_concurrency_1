"""Two threads cooking soup - this time creates custom thread by extending threading.thread.
Illustrates how the join() method works for threads."""

import threading
import time


class Chef1(threading.Thread):
    """Chef1 inherits from threading.Thread"""
    def __init__(self):
        """Use constructor from parent"""
        super().__init__()

    def run(self):
        """Simple method that waits for sausages to thaw and then chops them"""
        print(f"Chef1 thread name: {threading.current_thread().name}")
        print("Chef1 started and now waiting for sausages to thaw ...")
        time.sleep(3)
        print("Chef1 is finished chopping sausages.")


# Main thread
if __name__ == "__main__":
    print("Head chef started and requesting Chef1's help.")
    chef1 = Chef1()
    print(f" Chef1 alive? {chef1.is_alive()}")

    print("Head chef tells Chef1 to start.")
    chef1.start()
    print(f" Chef1 alive? {chef1.is_alive()}")

    print("Head shef continues cooking soup.")
    time.sleep(0.5)
    print(f" Chef1 alive? {chef1.is_alive()}")

    # This will block main thread for 2.5s
    print("Head chef waits for Chef1 to finish chopping sausages before continuing.")
    chef1.join()
    print(f" Chef1 alive? {chef1.is_alive()}")

    print("Head chef and Chef1 are both finished.")
