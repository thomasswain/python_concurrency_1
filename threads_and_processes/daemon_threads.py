"""Head chef cooks while cleaner cleans"""

import threading
import time


def kitchen_cleaner():
    """Repeatedly cleans the kitchen forever."""
    while True:
        print("Cleaner is cleaning the kitchen.")
        time.sleep(1)


if __name__ == "__main__":
    cleaner_thread = threading.Thread(target=kitchen_cleaner)
    # Without the following line, cleaner_thread will not terminate when main thread terminates
    cleaner_thread.setDaemon(True)

    cleaner_thread.start()

    print("Chef is cooking.")
    time.sleep(0.6)
    print("Chef is cooking.")
    time.sleep(0.6)
    print("Chef is cooking.")
    time.sleep(0.6)
    print("Chef is cooking.")
    time.sleep(0.6)
    print("Chef is finished.")
