"""Threads that waste CPU cycles"""

import os
import threading


def cpu_waster():
    """A simple function that wastes CPU cycles forever"""
    while True:
        pass


def display_process_info():
    """Display information about this process"""
    print("\nProcess ID: {}".format(os.getpid()))
    print("Thread Count: {}".format(threading.active_count()))
    for thread in threading.enumerate():
        print(thread)


display_process_info()

print("\nStarting 12 CPI Wasters ...")
for i in range(12):
    threading.Thread(target=cpu_waster).start()

display_process_info()
