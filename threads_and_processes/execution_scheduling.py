"""Two threads chopping vegetables - note in output that Thread 1/2 both chop very
different numbers of vegetables, seemingly random. This highlights how CPU time allocation
for threads is "unfair" and cannot be predicted."""

import threading
import time

chopping = True


def vegetable_chopper():
    """Get name of current thread, start chopping if chopping==True"""
    name = threading.current_thread().getName()
    vegetable_count = 0
    while chopping:
        print(f"{name} chopped a vegetable")
        vegetable_count += 1
    print(f"{name} chopped {vegetable_count} vegetables")


if __name__ == "__main__":
    """Create new threading.Thread objects, passing in a name and target method to run"""
    threading.Thread(target=vegetable_chopper, name="Thread 1").start()
    threading.Thread(target=vegetable_chopper, name="Thread 2").start()

    time.sleep(1)
    chopping = False
