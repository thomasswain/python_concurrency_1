"""Processes that waste CPU cycles"""

import os
import threading
import multiprocessing as mp


def cpu_waster():
    """A simple function that wastes CPU cycles forever"""
    while True:
        pass


def display_process_info():
    """Display information about this process"""
    print("\nProcess ID: {}".format(os.getpid()))
    print("Thread Count: {}".format(threading.active_count()))
    for thread in threading.enumerate():
        print(thread)


if __name__ == "__main__":
    display_process_info()

    print("\nStarting 12 CPI Wasters ...")
    for i in range(5):
        mp.Process(target=cpu_waster).start()

    display_process_info()
